package ru.t1.simanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.simanov.tm.api.service.IProjectTaskService;
import ru.t1.simanov.tm.api.service.IServiceLocator;
import ru.t1.simanov.tm.api.service.ITaskService;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.dto.response.*;
import ru.t1.simanov.tm.enumerated.Sort;
import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.exception.entity.TaskNotFoundException;
import ru.t1.simanov.tm.model.Task;

import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private ITaskService getTaskService() {
        return this.getServiceLocator().getTaskService();
    }

    private IProjectTaskService getProjectTaskService() {
        return this.getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse();
    }

    @NotNull
    @Override
    public TaskClearResponse taskClear(@NotNull TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse();
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse();
    }

    @NotNull
    @Override
    public TaskCreateResponse taskCreate(@NotNull TaskCreateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    public TaskListResponse taskList(@NotNull TaskListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        getTaskService().removeById(userId, task.getId());
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        getTaskService().removeById(userId, task.getId());
        return new TaskRemoveByIndexResponse();
    }

    @NotNull
    @Override
    public TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByProjectIdResponse taskShowByProjectId(@NotNull TaskShowByProjectIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse();
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse();
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse();
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse();
    }

}
