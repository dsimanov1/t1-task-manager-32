package ru.t1.simanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.dto.request.UserViewProfileRequest;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    public static final String NAME = "user-view-profile";

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest();
        @Nullable final User user = getAuthEndpoint().profile(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
