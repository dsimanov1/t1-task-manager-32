package ru.t1.simanov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IProjectEndpointClient extends IProjectEndpoint {

    void setSocket(@Nullable Socket socket);

}
