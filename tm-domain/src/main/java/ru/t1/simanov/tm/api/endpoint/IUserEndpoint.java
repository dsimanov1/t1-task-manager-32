package ru.t1.simanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse userLock(@NotNull UserLockRequest request);

    @NotNull
    UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse userRemove(@NotNull UserRemoveRequest request);

    @NotNull
    UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request);

    @NotNull
    UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request);

}
