package ru.t1.simanov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.simanov.tm.dto.request.UserLoginRequest;
import ru.t1.simanov.tm.dto.request.UserLogoutRequest;
import ru.t1.simanov.tm.dto.request.UserViewProfileRequest;
import ru.t1.simanov.tm.dto.response.UserLoginResponse;
import ru.t1.simanov.tm.dto.response.UserLogoutResponse;
import ru.t1.simanov.tm.dto.response.UserViewProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("ADMIN", "ADMIN")));
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserViewProfileResponse profile(@NotNull UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

}
