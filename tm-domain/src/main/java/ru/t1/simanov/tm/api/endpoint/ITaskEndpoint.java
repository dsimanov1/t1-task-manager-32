package ru.t1.simanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.dto.response.*;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse taskBindToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskChangeStatusByIdResponse taskChangeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse taskClear(@NotNull TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse taskCompleteById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse taskCompleteByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse taskCreate(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListResponse taskList(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse taskRemoveById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse taskRemoveByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskShowByIdResponse taskShowById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse taskShowByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskShowByProjectIdResponse taskShowByProjectId(@NotNull TaskShowByProjectIdRequest request);

    @NotNull
    TaskStartByIdResponse taskStartById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse taskStartByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskUnbindFromProjectResponse taskUnbindFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse taskUpdateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse taskUpdateByIndex(@NotNull TaskUpdateByIndexRequest request);

}
