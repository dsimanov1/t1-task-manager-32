package ru.t1.simanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.simanov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.simanov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.simanov.tm.dto.response.ApplicationVersionResponse;

public interface ISystemEndpoint {

    @NotNull ApplicationAboutResponse applicationAbout(@NotNull ApplicationAboutRequest request);

    @NotNull ApplicationVersionResponse applicationVersion(@NotNull ApplicationVersionRequest request);

}
