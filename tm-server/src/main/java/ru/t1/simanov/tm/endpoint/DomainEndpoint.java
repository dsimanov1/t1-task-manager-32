package ru.t1.simanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.simanov.tm.api.service.IDomainService;
import ru.t1.simanov.tm.api.service.IServiceLocator;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.dto.response.*;
import ru.t1.simanov.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IDomainService getDomainService() {
        return this.getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    public DataBackupLoadResponse dataBackupLoad(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse dataBackupSave(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse dataBase64Load(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse dataBase64Save(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse dataBinarySave(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse dataJsonLoadFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse dataJsonSaveFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse dataJsonLoadJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse dataJsonSaveJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse dataXmlLoadFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse dataXmlSaveFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse dataXmlLoadJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse dataXmlSaveJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse dataYamlLoadFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse dataYamlSaveFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataYamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
