package ru.t1.simanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    public static final String NAME = "project-clear";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        getProjectEndpoint().projectClear(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
