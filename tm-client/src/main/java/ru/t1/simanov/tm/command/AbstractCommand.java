package ru.t1.simanov.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.model.ICommand;
import ru.t1.simanov.tm.api.service.IServiceLocator;
import ru.t1.simanov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract String getName();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += " - " + description;
        return result;
    }

}
