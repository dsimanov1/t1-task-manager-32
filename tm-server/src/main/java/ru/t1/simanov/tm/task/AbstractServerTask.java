package ru.t1.simanov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected final Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
