package ru.t1.simanov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.endpoint.IUserEndpointClient;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.dto.response.*;

import java.net.Socket;

@NoArgsConstructor
public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    public UserEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("ADMIN", "ADMIN")));
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse userChangePassword(@NotNull UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse userLock(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse userRegistry(@NotNull UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse userRemove(@NotNull UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse userUnlock(@NotNull UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse userUpdateProfile(@NotNull UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @Override
    public void setSocket(@Nullable Socket socket) {

    }
}
