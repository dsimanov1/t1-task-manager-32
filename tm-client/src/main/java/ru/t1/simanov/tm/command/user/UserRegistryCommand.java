package ru.t1.simanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.dto.request.UserRegistryRequest;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "User registry.";

    @NotNull
    public static final String NAME = "user-registry";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @Nullable final User user = getUserEndpoint().userRegistry(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
