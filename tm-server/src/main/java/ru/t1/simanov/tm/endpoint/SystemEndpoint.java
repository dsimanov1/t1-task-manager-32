package ru.t1.simanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.api.service.IServiceLocator;
import ru.t1.simanov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.simanov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.simanov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.simanov.tm.dto.response.ApplicationVersionResponse;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public ApplicationAboutResponse applicationAbout(@NotNull ApplicationAboutRequest request) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    public ApplicationVersionResponse applicationVersion(@NotNull ApplicationVersionRequest request) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
