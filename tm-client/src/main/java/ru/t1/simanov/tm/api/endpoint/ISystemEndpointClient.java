package ru.t1.simanov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface ISystemEndpointClient extends ISystemEndpoint {

    void setSocket(@Nullable Socket socket);

}
