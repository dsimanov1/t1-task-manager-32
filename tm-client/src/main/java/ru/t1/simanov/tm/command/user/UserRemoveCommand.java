package ru.t1.simanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.UserRemoveRequest;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest();
        request.setLogin(login);
        getUserEndpoint().userRemove(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove User.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
