package ru.t1.simanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.DataXmlSaveJaxBRequest;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file.";

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest();
        getDomainEndpointClient().dataXmlSaveJaxB(request);
    }

}
