package ru.t1.simanov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.endpoint.*;
import ru.t1.simanov.tm.api.repository.IProjectRepository;
import ru.t1.simanov.tm.api.repository.ITaskRepository;
import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.api.service.*;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.endpoint.*;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.model.Project;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.repository.ProjectRepository;
import ru.t1.simanov.tm.repository.TaskRepository;
import ru.t1.simanov.tm.repository.UserRepository;
import ru.t1.simanov.tm.service.*;
import ru.t1.simanov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ApplicationAboutRequest.class, systemEndpoint::applicationAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::applicationVersion);
        server.registry(DataBackupLoadRequest.class, domainEndpoint::dataBackupLoad);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::dataBackupSave);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::dataBase64Load);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::dataBase64Save);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::dataBinaryLoad);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::dataBinarySave);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::dataJsonLoadFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::dataJsonLoadJaxB);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::dataJsonSaveFasterXml);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::dataJsonSaveJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::dataXmlLoadFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::dataXmlLoadJaxB);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::dataXmlSaveFasterXml);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::dataXmlSaveJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::dataYamlLoadFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::dataYamlSaveFasterXml);
        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::projectChangeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::projectChangeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::projectClear);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::projectCompleteById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::projectCompleteByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::projectCreate);
        server.registry(ProjectListRequest.class, projectEndpoint::projectList);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::projectRemoveById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::projectRemoveByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::projectShowById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::projectShowByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::projectStartById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::projectStartByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::projectUpdateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::projectUpdateByIndex);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::taskBindToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::taskChangeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::taskChangeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::taskClear);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::taskCompleteById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::taskCompleteByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::taskCreate);
        server.registry(TaskListRequest.class, taskEndpoint::taskList);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::taskRemoveById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::taskRemoveByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::taskShowById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::taskShowByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::taskShowByProjectId);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::taskStartById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::taskStartByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::taskUnbindFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::taskUpdateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::taskUpdateByIndex);
        server.registry(UserChangePasswordRequest.class, userEndpoint::userChangePassword);
        server.registry(UserLockRequest.class, userEndpoint::userLock);
        server.registry(UserRegistryRequest.class, userEndpoint::userRegistry);
        server.registry(UserRemoveRequest.class, userEndpoint::userRemove);
        server.registry(UserUnlockRequest.class, userEndpoint::userUnlock);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::userUpdateProfile);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final User test = userService.create("TEST", "TEST");

        projectService.add(test.getId(), new Project("First Project", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("Second Project", Status.NOT_STARTED));

        taskService.create(test.getId(), "First Task");
        taskService.create(test.getId(), "Second Task");
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
        server.stop();
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

}
