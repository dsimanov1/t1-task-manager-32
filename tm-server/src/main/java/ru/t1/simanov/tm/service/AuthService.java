package ru.t1.simanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.service.IAuthService;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.api.service.IUserService;
import ru.t1.simanov.tm.exception.field.LoginEmptyException;
import ru.t1.simanov.tm.exception.field.PasswordEmptyException;
import ru.t1.simanov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.simanov.tm.exception.user.UserLockedException;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull IPropertyService propertyService,
            @NotNull IUserService userService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User check(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.getLocked()) throw new UserLockedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new IncorrectLoginOrPasswordException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return user;
    }

}
