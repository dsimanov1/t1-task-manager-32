package ru.t1.simanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.UserLockRequest;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest();
        request.setLogin(login);
        getUserEndpoint().userLock(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Lock User.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-lock";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
