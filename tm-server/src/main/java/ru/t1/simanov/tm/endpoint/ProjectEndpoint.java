package ru.t1.simanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.simanov.tm.api.service.IProjectService;
import ru.t1.simanov.tm.api.service.IProjectTaskService;
import ru.t1.simanov.tm.api.service.IServiceLocator;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.dto.response.*;
import ru.t1.simanov.tm.enumerated.Sort;
import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.simanov.tm.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IProjectService getProjectService() {
        return this.getServiceLocator().getProjectService();
    }

    private IProjectTaskService getProjectTaskService() {
        return this.getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectClearResponse projectClear(@NotNull final ProjectClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse projectCompleteById(@NotNull final ProjectCompleteByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectCreateResponse projectCreate(@NotNull final ProjectCreateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    public ProjectListResponse projectList(@NotNull final ProjectListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull final ProjectRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectShowByIdResponse projectShowById(@NotNull final ProjectShowByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull final ProjectShowByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse projectStartById(@NotNull final ProjectStartByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull final ProjectStartByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull final ProjectUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse();
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse();
    }

}
