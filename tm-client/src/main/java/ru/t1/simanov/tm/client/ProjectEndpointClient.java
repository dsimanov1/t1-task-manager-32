package ru.t1.simanov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.endpoint.IProjectEndpointClient;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.dto.response.*;

import java.net.Socket;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpointClient {

    public ProjectEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("ADMIN", "ADMIN")));
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());
        @NotNull final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient(authEndpointClient);
        System.out.println(projectEndpointClient.projectCreate(new ProjectCreateRequest("Fourth Project", "Fourth Project description")));
        System.out.println(projectEndpointClient.projectList(new ProjectListRequest()).getProjects());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectClearResponse projectClear(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIdResponse projectCompleteById(@NotNull final ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull final ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCreateResponse projectCreate(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectListResponse projectList(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIdResponse projectShowById(@NotNull final ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull final ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIdResponse projectStartById(@NotNull final ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull final ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

    @Override
    public void setSocket(@Nullable Socket socket) {

    }

}
