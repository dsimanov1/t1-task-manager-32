package ru.t1.simanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.TaskRemoveByIndexRequest;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove task by index.";

    @NotNull
    public static final String NAME = "task-remove-by-index";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest();
        request.setIndex(index);
        getTaskEndpoint().taskRemoveByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
