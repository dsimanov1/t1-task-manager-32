package ru.t1.simanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.DataBackupLoadRequest;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load backup from file.";

    @NotNull
    public static final String NAME = "backup-load";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest();
        getDomainEndpointClient().dataBackupLoad(request);
    }

}
