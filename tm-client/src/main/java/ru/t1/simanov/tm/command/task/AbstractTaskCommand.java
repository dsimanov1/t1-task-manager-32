package ru.t1.simanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.endpoint.ITaskEndpointClient;
import ru.t1.simanov.tm.command.AbstractCommand;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskEndpointClient getTaskEndpoint() {
        return serviceLocator.getTaskEndpointClient();
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    public void renderTasks(@NotNull final List<Task> tasks) {
        for (@NotNull final Task task : tasks) {
            showTask(task);
            System.out.println();
        }
    }

    public void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + task.getCreated());
    }


}
