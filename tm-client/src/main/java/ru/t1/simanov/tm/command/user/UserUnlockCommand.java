package ru.t1.simanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.UserUnlockRequest;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest();
        request.setLogin(login);
        getUserEndpoint().userUnlock(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unlock User.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
