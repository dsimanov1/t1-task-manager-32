package ru.t1.simanov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
